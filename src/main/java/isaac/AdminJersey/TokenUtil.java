package isaac.AdminJersey;

import java.security.Key;
import java.util.Date;

import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

public class TokenUtil {
	private Key key;
	private static TokenUtil tokenUtil;
	public TokenUtil() {}
	public static TokenUtil getInstance() {
		if(tokenUtil==null) {
			tokenUtil = new TokenUtil();
		}
		return tokenUtil;
	}
	public String GenerateToken(String subject) {
		
		Date expiration = new Date(); 
		expiration.setTime(new Date().getTime()+1000*60*60);
		String compactJws = Jwts.builder()
				  .setSubject(subject)//设置主题
				  .signWith(GetKey())
				  .setExpiration(expiration)
				  .compact();
		return compactJws;
	}
	private Key GetKey() {
		if(key==null) {
			key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
		}
		return key;
	}
	
	public String parseToken(String jws) {
		try {
			String JWSparse = Jwts.parser().setSigningKey(GetKey()).parseClaimsJws(jws).getBody().getSubject();
		    return JWSparse;

		} catch (JwtException e) {
			return null;
		}
	}
}
