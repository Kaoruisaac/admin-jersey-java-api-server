package isaac.AdminJersey.app;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import org.jongo.Jongo;
import org.jongo.MongoCursor;

import isaac.AdminJersey.MongoCore;
import isaac.AdminJersey.TokenUtil;
import isaac.AdminJersey.admin.model.AccountModel;
import isaac.AdminJersey.admin.model.RoleConfig;
import isaac.AdminJersey.app.model.ArticleModel;
import isaac.AdminJersey.app.model.ImageUploadModel;
import isaac.AdminJersey.share.model.PaginationModel;
import isaac.AdminJersey.share.model.StatusConfig;
import isaac.AdminJersey.share.model.StatusModel;

@Path("/article")
public class ArticleApp {
	Jongo jongo;	
	public ArticleApp() {
		jongo = MongoCore.getInstance().getJongo();
	}
	
	@POST
	@Path("/imageUpload")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Object imageUpload(ImageUploadModel ImageUpload) {
		String path = "/uploads/"+UUID.randomUUID()+"."+ImageUpload.type;
		try{
			byte[] decodedImg = Base64.getDecoder().decode(ImageUpload.image);
			OutputStream stream = new FileOutputStream("c:/xampp/tomcat/webapps"+path);
			stream.write(decodedImg);
		}catch(Exception e) {
			return new StatusModel(StatusConfig.FAILD);
		}
		
		StatusModel status = new StatusModel(StatusConfig.SUCCESS);
		status.data = "c:/xampp/tomcat"+path;
		return status;
	}
	
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Object Add(ArticleModel article,@Context HttpHeaders httpheaders) {
		// verify token
		String accountUID = TokenUtil.getInstance().parseToken(httpheaders.getHeaderString("Authorization"));
		StatusModel status;
		if(accountUID == null) {
			status = new StatusModel(StatusConfig.TOKEN_EXPIRED);
			return status;
		}
		
		article.uid = UUID.randomUUID().toString();
		article.authorId = accountUID;
		try {
			jongo.getCollection("article").insert(article);
		}catch(Exception e){
			return new StatusModel(StatusConfig.FAILD);
		}

		
		status = new StatusModel(StatusConfig.SUCCESS);
		return status;
	}
	
	@POST
	@Path("/getList")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getList(PaginationModel pagi) {
		
		StatusModel status;
		MongoCursor<ArticleModel> accountCursor;
		try {
			pagi.totalItems = (int) jongo.getCollection("account").count();
			pagi.totalPage = (int)Math.ceil((double)pagi.totalItems / (double)pagi.size);
			accountCursor = jongo.getCollection("article").find().skip(pagi.size*(pagi.currentPage-1)).limit(pagi.size).as(ArticleModel.class);
		}catch(Exception e) {
			return new StatusModel(StatusConfig.FAILD);
		}
		ArrayList<ArticleModel> dataList = new ArrayList<ArticleModel>();
		while(accountCursor.hasNext()) {
			dataList.add(accountCursor.next());
		}
		status = new StatusModel(StatusConfig.SUCCESS);
		pagi.dataList = dataList;
		status.data = pagi;
		return status;
	}
	
	@POST
	@Path("/get/{uid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getArticleById(@PathParam("uid") String UID) {
		StatusModel status;
		ArticleModel article;
		try {
			article = jongo.getCollection("article").findOne("{uid:'"+ UID +"'}").as(ArticleModel.class);
		}catch(Exception e) {
			return new StatusModel(StatusConfig.FAILD);
		}
		
		status = new StatusModel(StatusConfig.SUCCESS);
		status.data = article;
		return status;
	}
	
	@POST
	@Path("/delete/{uid}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Object delete(@Context HttpHeaders httpheaders, @PathParam("uid") String UID) {

		// verify token
		String accountUID = TokenUtil.getInstance().parseToken(httpheaders.getHeaderString("Authorization"));
		StatusModel status;
		if(accountUID == null) {
			status = new StatusModel(StatusConfig.TOKEN_EXPIRED);
			return status;
		}
		
		//remove
		try {
			jongo.getCollection("article").remove("{uid:'"+ UID +"'}");
		}catch(Exception e) {
			return new StatusModel(StatusConfig.FAILD);
		}
		
		status = new StatusModel(StatusConfig.SUCCESS);
		return status;
		
	}
}
