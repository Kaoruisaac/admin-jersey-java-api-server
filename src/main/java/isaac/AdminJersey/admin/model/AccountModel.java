package isaac.AdminJersey.admin.model;

public class AccountModel {
	public AccountModel() {}
	public AccountModel(AccountModel account) {
		// Duplicate account without password
		uid = account.uid;
		user = account.user;
		name = account.name;
		email = account.email;
		phone = account.phone;
		role = account.role;
		Token = account.Token;
	}
	public String uid;
	public String user;
	public String password;
	public String newPassword;
	public String phone;
	public String name;
	public String email;
	public Integer role;
	public String Token;
}