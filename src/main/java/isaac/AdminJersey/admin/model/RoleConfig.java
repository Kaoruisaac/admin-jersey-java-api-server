package isaac.AdminJersey.admin.model;

import java.util.ArrayList;

import isaac.AdminJersey.share.model.TextValueModel;

public class RoleConfig {
	public static Integer SUPER_ADMIN = 1;
	public static Integer ACCOUNTANT = 2;
	public static Integer NORMAL_STAFF = 3;
	
	public ArrayList<Object> getList(){
		ArrayList<Object> list = new ArrayList<>();
		list.add(new TextValueModel("Admin",SUPER_ADMIN));
		list.add(new TextValueModel("Accountant",ACCOUNTANT));
		list.add(new TextValueModel("Staff",NORMAL_STAFF));
		return list;
	}
}
