package isaac.AdminJersey.admin;

import java.util.ArrayList;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import org.apache.commons.codec.digest.DigestUtils;
import org.jongo.Jongo;
import org.jongo.MongoCursor;

import com.google.gson.Gson;

import isaac.AdminJersey.MongoCore;
import isaac.AdminJersey.TokenUtil;
import isaac.AdminJersey.admin.model.AccountModel;
import isaac.AdminJersey.admin.model.RoleConfig;
import isaac.AdminJersey.share.model.LoginOptionModel;
import isaac.AdminJersey.share.model.PaginationModel;
import isaac.AdminJersey.share.model.StatusConfig;
import isaac.AdminJersey.share.model.StatusModel;


@Path("/account")
public class AccountAdmin {
	
	Jongo jongo;	
	public AccountAdmin() {
		jongo = MongoCore.getInstance().getJongo();
	}
	
	@GET
	@Path("/getRoles")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getRoles() {
		StatusModel status;
		status = new StatusModel(StatusConfig.SUCCESS);
		status.data = new RoleConfig().getList();
		return status;
	}
	
	@POST
	@Path("/login")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Object login(LoginOptionModel loginOption) {
		// get account
		AccountModel account = jongo.getCollection("account").findOne("{user:'"+ loginOption.user +"'}").as(AccountModel.class);
		if(account==null) {
			account = jongo.getCollection("account").findOne("{email:'"+ loginOption.user +"'}").as(AccountModel.class);
		}
		if(account==null) {
			return new StatusModel(StatusConfig.NO_ACCOUNT);
		}
		// verify password
		if(!account.password.equals(DigestUtils.md5Hex(loginOption.password))) {
			return new StatusModel(StatusConfig.ACCOUNT_PASSWORD_ERROR);
		}
		
		
		StatusModel status = new StatusModel(StatusConfig.SUCCESS);
		account.Token = TokenUtil.getInstance().GenerateToken(account.uid);
		status.data = new AccountModel(account);
		
		return status;
	}
	
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Object add(AccountModel account,@Context HttpHeaders httpheaders) {
		// verify token
		String accountUID = TokenUtil.getInstance().parseToken(httpheaders.getHeaderString("Authorization"));
		StatusModel status;
		if(accountUID == null) {
			status = new StatusModel(StatusConfig.TOKEN_EXPIRED);
			return status;
		}
		
		// check permission
		Integer ROLE = null;
		try {
			ROLE = jongo.getCollection("account").findOne("{uid:'"+ accountUID +"'}").as(AccountModel.class).role;
		}catch(Exception e){
			return new StatusModel(StatusConfig.FAILD);
		}
		if(ROLE != RoleConfig.SUPER_ADMIN) {
			return new StatusModel(StatusConfig.PERMISSION_DENIED);
		}
		
		// check field empty
		if(account.user==null||account.newPassword==null||account.role==null) {
			status= new StatusModel(StatusConfig.FAILD);
			status.msg = "account/password/role can't be empty";
			return status;
		}
		
		// check user existed
		try {
			AccountModel existUser = jongo.getCollection("account").findOne("{user:'"+ account.user +"'}").as(AccountModel.class);
			AccountModel existEmail = jongo.getCollection("account").findOne("{email:'"+ account.email +"'}").as(AccountModel.class);
			if(existUser!=null) {
				status= new StatusModel(StatusConfig.FAILD);
				status.msg = "User existed!";
				return status;
			}
			if(existEmail!=null) {
				status= new StatusModel(StatusConfig.FAILD);
				status.msg = "Email hsa been registered!";
				return status;
			}
		}catch(Exception e){
			return new StatusModel(StatusConfig.FAILD);
		}
		
		
		//insert
		account.uid = UUID.randomUUID().toString();
		account.password = DigestUtils.md5Hex(account.newPassword);
		try {
			jongo.getCollection("account").insert(account);
		}catch(Exception e){
			return new StatusModel(StatusConfig.FAILD);
		}
		
		
		status= new StatusModel(StatusConfig.SUCCESS);
		status.data = new AccountModel(account);
		return status;
	}
	
	@POST
	@Path("/getInfo")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getInfo(@Context HttpHeaders httpheaders) {
		String accountUID = TokenUtil.getInstance().parseToken(httpheaders.getHeaderString("Authorization"));
		StatusModel status;
		if(accountUID == null) {
			status = new StatusModel(StatusConfig.TOKEN_EXPIRED);
			return status;
		}
		
		return selfGetInfo(accountUID);
	}
	
	@POST
	@Path("/getInfo/{uid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getInfoByUid(@Context HttpHeaders httpheaders, @PathParam("uid") String UID) {
		String accountUID = TokenUtil.getInstance().parseToken(httpheaders.getHeaderString("Authorization"));
		StatusModel status;
		if(accountUID == null) {
			status = new StatusModel(StatusConfig.TOKEN_EXPIRED);
			return status;
		}
		
		if(accountUID.equals(UID)) {
			return selfGetInfo(UID);
		}else {
			return adminGetInfo(UID,accountUID);
		}
		
	}
	
	public Object selfGetInfo(String uid) {
		StatusModel status;
		AccountModel account;
		try {
			account = jongo.getCollection("account").findOne("{uid:'"+ uid +"'}").as(AccountModel.class);
		}catch(Exception e) {
			return new StatusModel(StatusConfig.FAILD);
		}
		
		status = new StatusModel(StatusConfig.SUCCESS);
		status.data = new AccountModel(account);
		return status;
	}
	
	public Object adminGetInfo(String uid,String accountUID) {
		//check permission
		Integer ROLE = null;
		try {
			ROLE = jongo.getCollection("account").findOne("{uid:'"+ accountUID +"'}").as(AccountModel.class).role;
		}catch(Exception e){
			return new StatusModel(StatusConfig.FAILD);
		}
		
		if(ROLE != RoleConfig.SUPER_ADMIN) {
			return new StatusModel(StatusConfig.PERMISSION_DENIED);
		}
		
		StatusModel status;
		AccountModel account;
		try {
			account = jongo.getCollection("account").findOne("{uid:'"+ uid +"'}").as(AccountModel.class);
		}catch(Exception e){
			return new StatusModel(StatusConfig.FAILD);
		}
		
		status = new StatusModel(StatusConfig.SUCCESS);
		status.data = new AccountModel(account);
		return status;
	}
	
	@POST
	@Path("/getList")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getList(@Context HttpHeaders httpheaders, PaginationModel pagi) {
		// verify token
		String accountUID = TokenUtil.getInstance().parseToken(httpheaders.getHeaderString("Authorization"));
		StatusModel status;
		if(accountUID == null) {
			status = new StatusModel(StatusConfig.TOKEN_EXPIRED);
			return status;
		}
		
		MongoCursor<AccountModel> accountCursor;
		try {
			pagi.totalItems = (int) jongo.getCollection("account").count();
			pagi.totalPage = (int)Math.ceil((double)pagi.totalItems / (double)pagi.size);
			accountCursor = jongo.getCollection("account").find().skip(pagi.size*(pagi.currentPage-1)).limit(pagi.size).as(AccountModel.class);
		}catch(Exception e) {
			return new StatusModel(StatusConfig.FAILD);
		}
		ArrayList<AccountModel> dataList = new ArrayList<AccountModel>();
		while(accountCursor.hasNext()) {
			dataList.add(new AccountModel(accountCursor.next()));
		}
		status = new StatusModel(StatusConfig.SUCCESS);
		pagi.dataList = dataList;
		status.data = pagi;
		return status;
	}
	
	@POST
	@Path("/modify")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Object modify(AccountModel account,@Context HttpHeaders httpheaders) {
		//verify token
		String accountUID = TokenUtil.getInstance().parseToken(httpheaders.getHeaderString("Authorization"));
		StatusModel status;
		if(accountUID == null) {
			status = new StatusModel(StatusConfig.TOKEN_EXPIRED);
			return status;
		}
		
		if(accountUID.equals(account.uid)||account.uid == null) {
			return selfModify(account,accountUID);
		}else {
			return adminModify(account,accountUID);
		}
	}
	
	public AccountModel modifyProcessing(AccountModel account) {
		AccountModel editDetail = new AccountModel();
		if(account.newPassword != null &&!account.newPassword.isEmpty()) {
			editDetail.password = DigestUtils.md5Hex(account.newPassword);
		}
		if(account.name != null) {
			editDetail.name = account.name;
		}
		if(account.email != null) {
			editDetail.email = account.email;
		}
		if(account.phone != null) {
			editDetail.phone = account.phone;
		}
		return editDetail;
	}
	
	public Object selfModify(AccountModel account,String accountUID) {
		StatusModel status;
		
		/*modify start*/
		try {
			jongo.getCollection("account").update("{uid:'"+ accountUID +"'}").with(modifyProcessing(account));
		}catch(Exception e){
			return new StatusModel(StatusConfig.FAILD);
		}
		/*modify end*/
		
		status = new StatusModel(StatusConfig.SUCCESS);
		return status;
	}
	
	public Object adminModify(AccountModel account, String adminUID) {
		//check permission
		Integer ROLE = null;
		try {
			ROLE = jongo.getCollection("account").findOne("{uid:'"+ adminUID +"'}").as(AccountModel.class).role;
		}catch(Exception e){
			return new StatusModel(StatusConfig.FAILD);
		}
		
		if(ROLE != RoleConfig.SUPER_ADMIN) {
			return new StatusModel(StatusConfig.PERMISSION_DENIED);
		}
		
		
		/*modify start*/
		try {
			jongo.getCollection("account").update("{uid:'"+ account.uid +"'}").with(modifyProcessing(account));
		}catch(Exception e){
			return new StatusModel(StatusConfig.FAILD);
		}
		/*modify end*/
		
		StatusModel status = new StatusModel(StatusConfig.SUCCESS);
		return status;
	}
	
	@POST
	@Path("/delete/{uid}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Object delete(@Context HttpHeaders httpheaders, @PathParam("uid") String UID) {
		String accountUID = TokenUtil.getInstance().parseToken(httpheaders.getHeaderString("Authorization"));
		StatusModel status;
		if(accountUID == null) {
			status = new StatusModel(StatusConfig.TOKEN_EXPIRED);
			return status;
		}
		
		//check permission
		Integer ROLE = null;
		try {
			ROLE = jongo.getCollection("account").findOne("{uid:'"+ accountUID +"'}").as(AccountModel.class).role;
		}catch(Exception e){
			return new StatusModel(StatusConfig.FAILD);
		}
			
		if(ROLE != RoleConfig.SUPER_ADMIN) {
			return new StatusModel(StatusConfig.PERMISSION_DENIED);
		}
		
		//remove account
		try {
			jongo.getCollection("account").remove("{uid:'"+ UID +"'}");
		}catch(Exception e) {
			return new StatusModel(StatusConfig.FAILD);
		}
		
		status = new StatusModel(StatusConfig.SUCCESS);
		return status;
		
	}
}