package isaac.AdminJersey.share.model;

public interface StatusConfig {
	public int SUCCESS = 1000;
	public int FAILD = 1001;
	public int NO_ACCOUNT = 1002;
	public int ACCOUNT_PASSWORD_ERROR = 1003;
	public int TOKEN_EXPIRED = 1004;
	public int PERMISSION_DENIED = 1005;
}