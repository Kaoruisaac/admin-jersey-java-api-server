package isaac.AdminJersey.share.model;


public class StatusModel {
	public StatusModel(int CODE) {
		switch(CODE) {
			case StatusConfig.SUCCESS :
				msg = "API called successfully!";
				break;
			case StatusConfig.FAILD :
				msg = "API server error!";
				break;
			case StatusConfig.NO_ACCOUNT :
				msg = "Account doesn't exist!";
				break;
			case StatusConfig.ACCOUNT_PASSWORD_ERROR :
				msg = "Wrong password!";
				break;
			case StatusConfig.TOKEN_EXPIRED :
				msg = "Token expired.";
				break;
			case StatusConfig.PERMISSION_DENIED :
				msg = "permission denied.";
				break;
		}
		code = CODE;
	}
	public Object data;
	public String msg;
	public int code;
}

