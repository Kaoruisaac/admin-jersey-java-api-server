package isaac.AdminJersey;

import org.jongo.Jongo;

import com.mongodb.DB;
import com.mongodb.MongoClient;

public class MongoCore{
	static MongoCore mongoCore;
	static MongoClient mongoClient;
   
	private static DB db;
	private static final String dbHost = "127.0.0.1";
	private static final int dbPort = 27017;
	private static final String dbName = "mycol";
	private static Jongo jongo;
	
	public MongoCore() {}
	
	public static MongoCore getInstance() {
		if(mongoCore== null) {
			mongoCore = new MongoCore();
		}
		return mongoCore;
	}
	
	@SuppressWarnings("deprecation")
	private DB getDB() {
		if(mongoClient==null) {
			mongoClient = new MongoClient(dbHost , dbPort);
		}
		if(db == null) {
			db = mongoClient.getDB(dbName);
		}
		return db;
	}
	public Jongo getJongo() {
		if(jongo==null) {
			jongo = new Jongo(getDB());
		}
		return jongo;
	}
}